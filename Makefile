CC=gcc
CXX=g++
CFLAGS=-std=c11
CXXFLAGS=-std=c++14
LDFLAGS := -lm
OUT := $(strip $(foreach f,*.c*,$(wildcard $(f))))
% : %.c
	$(CC) $(CFLAGS) $< -o $@.exe $(LDFLAGS)

clean:
	@rm *.exe
	@for F in ${OUT}; do \
		rm -f "$${F%%.*}"; \
	done
