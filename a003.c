#include <stdio.h>

int main (int argc, char* argv[])
{
  int a, b;
  char m[3][10] = { "普通", "吉", "大吉" };
  while (scanf ("%d %d", &a, &b) == 2) {
    printf ("%s\n", m[(a*2+b)%3]);
  }
  return 0;
}
