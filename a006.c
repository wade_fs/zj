#include <stdio.h>
#include <math.h>

int main (int argc, char* argv[])
{
  int a, b, c;

  while (scanf ("%d %d %d", &a, &b, &c) == 3) {
    double r = (double)((b*b) - 4*(a*c));
    if (r == 0.0) {
      printf ("Two same roots x=%.0lf\n", (double)(-b/(2.0*a)));
    } else if (r > 0.0) {
      double s = sqrt(r);
      printf ("Two different roots x1=%.0lf , x2=%.0lf\n", (double)((-b+s)/(2.0*a)), (double)((-b-s)/(2.0*a)));
    } else {
      printf ("No real root\n");
    }
  }
  return 0;
}
