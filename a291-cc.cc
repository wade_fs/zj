#include <iostream>
#include <string.h>

void judge(int answer[], int input[]) {
  int a=0, b=0;
  int ansFlag[4]; // 因為我需要改動 answer[] 的值，所以做備份，也可以想成是獨立的旗標，已比對到就不再參與比對
  memcpy(ansFlag, answer, 4*sizeof(int));

  /* 首先比對相同位置 */
  for (int i=0; i<4; ++i) {
    // 如果相同就是 a++;
    if (ansFlag[i] == input[i]) {
      ansFlag[i] = -1; input[i] = -2;  // 此時，比對到的位置設為 -1，這樣下次就不會被比對到
      a++;
    }
  }

  /* 拿 answer 跟 input 比對 */
  for (int j=0; j<4; ++j) { // answer
    for (int i=0; i<4; ++i) { // input 
      // 略過位置相同的部份....如果數字相同, 那就是 b++
      if (i != j && ansFlag[j] == input[i]) {
        b++;
        ansFlag[j] = -1; input[i] = -2; // 此時，一樣要把輸入位置設為 -1，這樣就不會再被比對到
      }
    }
  }
  printf ("%dA%dB\n", a, b);
}

int main()
{
  int answer[4], input[4], n;

  while (scanf("%d %d %d %d %d", answer, answer+1, answer+2, answer+3, &n) != EOF) {
    for (int i=0; i<n; ++i) {
      scanf("%d %d %d %d", input, input+1, input+2, input+3);
      judge(answer, input);
    }
  }
  return 0;
}
