#include<stdio.h>
#include<math.h>

int main ()
{
  int n, nn, last, cnt;

  while (scanf ("%d", &n) == 1) {
    last = -1;
    cnt=1;
    nn = sqrt(n);
    while (!(n % 2)) { // 因為所有質因數只有2是偶數，先處理2
      n >>= 1;
      if (last != 2) {
        printf ("2");
        last = 2;
        cnt = 1;
      } else cnt++;
    }
    if (cnt > 1) {
      printf ("^%d", cnt);
      cnt = 1;
    }
    for (int i = 3; i <= nn;) { // 之後所有質因數必定是奇數
      if (n % i == 0) { // 看有沒有重覆的質因數
        n /= i;
        if (last != i) { // 有質因數，但是與前一個不同
          if (cnt > 1) printf ("^%d * %d", cnt, i); // 次方
          else { // 非次方
            if (last <= 0) printf ("%d", i); // 第一筆
            else printf (" * %d", i); // 非第一筆
          }
          cnt = 1;
          last = i;
        } else cnt++; // 有質因數，且與前一次相同，表示多次相乘(次方)
      } else i += 2; // 如果沒有質因數，每次加2
    }
    // 最後有四種情況
    if (cnt > 1) {
      if (n > 1) printf ("^%d * %d\n", cnt, n); // 前面為次方(cnt>1) 且 最後有非1質因數(n>1)
      else printf ("^%d\n", cnt); // 前面為次方(cnt>1)，但是也是結尾(n==1)
    } else if (n > 1) {
      if (last <= 0) printf ("%d\n", n); // 前面非次方(cnt==1) 且 最後有非1質因數(n>1)
      else printf (" * %d\n", n); // 前面非次方(cnt==1) ，但是結尾也是質因數(n>1)
    } else puts(""); // 前面非次方，結尾也是1，也就是結束了，印換行
  }
  return 0;
}
