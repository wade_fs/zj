#!/usr/bin/python3
from sys import stdin

for line in stdin:
  if len(line.strip()) == 0: continue
  answer = line.split()
  n = int(input())

  for r in range(n):
    a = b = 0
    ansFlag = answer[:]

    Q = input().split()
    for i in range(4):
      if ansFlag[i] == Q[i]:
        a += 1
        ansFlag[i] = -1
        Q[i] = -2
    for j in range(4):
      for i in range(4):
        if i != j and ansFlag[j] == Q[i]:
          b += 1
          ansFlag[j] = -1
          Q[i] = -2
    print ("%dA%dB"%(a, b))
