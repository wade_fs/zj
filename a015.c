#include <stdio.h>

int main ()
{
  int m, n;
  int a[100][100];

  while (scanf ("%d %d", &m, &n) == 2) {
    for (int j=0; j<m; ++j)
      for (int i=0; i<n; ++i)
        scanf("%d", &a[j][i]);
    for (int j=0; j<n; ++j) {
      for (int i=0; i<m-1; ++i)
        printf ("%d ", a[i][j]);
      printf ("%d\n", a[m-1][j]);
    }
  }
  return 0;
}
