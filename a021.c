#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX   99999999
#define UMAX 100000000
#define N 10
typedef struct {
  int d[N];
} BIG;

void bigInit(char*, BIG*);
void toComp(BIG*, BIG);
int isZero(BIG);
void inc(BIG*);
void absv(BIG*, BIG);
int compare(BIG, BIG);
void add(BIG, BIG, BIG*);
void sub(BIG, BIG, BIG*);
void bigSet(BIG*, BIG);
void intMul(BIG, int, BIG*);
void bigMul(BIG, BIG, BIG*);

void bigDiv(BIG, BIG, BIG*);
void modulo(BIG, BIG, BIG*);

/* 視為正號來比對 */
int compare(BIG a, BIG b) {
  if (a.d[0] == MAX) {
    if (b.d[0] == MAX) {
      absv(&a, a); absv(&b, b);
      return compare(b, a);
    } else return -1;
  } else if (b.d[0] == MAX) return 1;
  // 至此皆正數
  else {
    for (int i=0; i<N; ++i) if (a.d[i] != b.d[i]) return a.d[i] - b.d[i];
  }
  
  return 0;
}

void bigShiftLeft(BIG* c, BIG a, int l) {
  if (l >= N) bigInit("", c);
  else {
    int i, n = a.d[0] == MAX;
    if (n) absv(&a, a);
    for (i=l; i<N; ++i) {
      c->d[i-l] = a.d[i];
    }
    for (; i-l<N; ++i) {
      c->d[i-l] = 0;
    }
    if (n) {
      toComp(c, *c);
      inc(c);
    }
  }
}

void bigShiftRight(BIG* c, BIG a, int r) {
  if (r >= N) bigInit("", c);
  else {
    int i, n = a.d[0] == MAX;
    if (n) absv(&a, a);
    for (i=N-r-1; i>0; --i) {
      c->d[i+r] = a.d[i];
    }
    for (; i+r >= 0; --i) {
      c->d[i+r] = 0;
    }
    if (n) {
      toComp(c, *c);
      inc(c);
    }
  }
}

void bigDiv(BIG a, BIG b, BIG* c) {
  if (isZero(a)) { bigInit("", c); }
  else {
    int n = b.d[0] == MAX && a.d[0] == 0 || b.d[0] == 0 && a.d[0] == MAX;
    absv(&a, a);
    absv(&b, b);
    int cp = compare(a, b);
    if (cp == 0) bigInit("1", c);
    else if (cp < 0) bigInit("", c);
    else {
      // 沒想通，應該超複雜....
      // 意思是，不能用這邊的想法，搞不好用 string 來表達才對
      BIG d;
      bigSet(&d, a);
      bigInit("1", c);
      while (compare(d, b) > 0) {
        sub(d, b, &d);
        inc(c);
      }
    }
    if (n) {
      toComp(c, *c);
      inc(c);
    }
  }
}

void intMul(BIG a, int b, BIG* c) {
  int n = b < 0 && a.d[0] == 0 || b > 0 && a.d[0] == MAX;
  long carry=0, tmp, l;
  if (b == 0) bigInit("", c);
  else if (b == 1) bigSet(c, a);
  else if (b == -1) {
    toComp(c, a);
    inc(c);
  } else {
    if (b < 0) b = -b;
    absv(&a, a);
    for (int i=N-1; i>=0; --i) {
      tmp = (long)a.d[i] * b + carry;
      c->d[i] = tmp % UMAX;
      carry = tmp / UMAX;
    }
    if (n) {
      toComp(c, *c);
      inc(c);
    }
  }
}

void bigMul(BIG a, BIG b, BIG* c) {
  BIG tmp, aa;
  int n = a.d[0] == MAX && b.d[0] != MAX || a.d[0] != MAX && b.d[0] == MAX;
  if (a.d[0] == MAX) absv(&a, a);
  if (b.d[0] == MAX) absv(&b, b);
  bigInit("", &tmp);
  bigInit("", c);
  bigSet(&aa, a);
  for (int i=N-1; i>=0; --i) {
    intMul(aa, b.d[i], &tmp);
    add(*c, tmp, c);
    bigShiftLeft(&aa, aa, 1);
  }
  if (n) {
    toComp(c, *c);
    inc(c);
  }
}

void modulo(BIG a, BIG b, BIG* c) {
}

void bigInit(char* s, BIG* b) {
  memset(b->d, 0, sizeof(b->d));
  if (strlen(s) > 0) {
    char* c;
    int minus = 0;
    if (s[0] == '-') {
      c = (char*)calloc(strlen(s), sizeof(char));
      minus = 1;
      strcpy(c, s+1);
    } else {
      c = (char*)calloc(strlen(s)+1, sizeof(char));
      strcpy(c, s);
    }

    // 從右往左看，每9個數字存一個 uint32_t 元素, 原因是 uint32_t 有10位數
    char* p = c+strlen(c);
    int n=N-1;
    while (p > c) {
      p -= 8;
      if (p < c) p = c;
      b->d[n] = atoi(p);
      *p = '\0'; --n;
    }
    free(c);
    if (minus) {
      toComp(b, *b); inc(b);
    }
  }
}

void inc(BIG* c) {
  int carry = 1;
  for (int i=N-1; i>=0; --i) {
    c->d[i] = c->d[i] + carry;
    if (c->d[i] >= UMAX) {
      carry = 1;
      c->d[i] = 0;
    } else carry = 0;
  }
}

void toComp(BIG* c, BIG b) {
  for (int i=0; i<N; ++i) {
    c->d[i] = MAX - b.d[i];
  }
}

void absv(BIG* c, BIG b) {
  if (b.d[0] == MAX) { toComp(c, b); inc(c); }
  else {
    for (int i=0; i<N; ++i) c->d[i] = b.d[i];
  }
}

int isZero(BIG b) {
  int r = 1;
  for (int i=0; i<N; i++) {
    if (b.d[i] != 0) { r = 0; break; }
  }
  return r;
}

void bigSet(BIG* a, BIG b) {
  memcpy(a->d, b.d, sizeof(b.d));
}

void bigPrint(BIG b) {
  BIG c;
  if (isZero(b)) {
    printf("0");
    return;
  }
  if (b.d[0] == MAX) {
    printf ("-");
    toComp(&c, b);
    inc(&c);
  } else bigSet(&c, b);
  int skip = 1, i;
  for (i=0; i<N; ++i) {
    if (skip && c.d[i] == 0) continue;
    else {
      printf ("%d", c.d[i]);
      skip = 0;
      break;
    }
  }
  ++i;
  for (; i<N; ++i) {
    printf ("%08d", c.d[i]);
  }
}

void add(BIG a, BIG b, BIG* c) {
  if (b.d[0] == MAX) {
    toComp(&b, b);
    inc(&b);
    sub(a, b, c);
  } else {
    int carry = 0;
    for (int i=N-1; i>=0; --i) {
      c->d[i] = a.d[i] + b.d[i] + carry;
      if (c->d[i] >= UMAX) {
        carry = 1;
        c->d[i] -= UMAX;
      } else carry = 0;
    }
  }
}

void sub(BIG a, BIG b, BIG* c) {
  if (b.d[0] == MAX) {
    toComp(&b, b);
    inc(&b);
    add(a, b, c);
  } else {
    int borrow = 0;
    for (int i=N-1; i>=0; --i) {
      c->d[i] = a.d[i] - b.d[i] - borrow;
      if (c->d[i] < 0) {
        borrow = 1;
        c->d[i] += UMAX;
      } else { borrow = 0; }
    }
  }
}

int main ()
{
  BIG a;
  BIG b;
  BIG c;
  int bb;
  char buf1[1024], op[3], buf2[1024], buf3[1024];

  while (!feof(stdin)) {
    scanf("%1023[^\n]\n", buf3);
    // if (sscanf(buf3, "%1024[^ ] %1024[^ ] %1024[^ ]\n", buf1, op, buf2) == 3) {
    // if (sscanf(buf3, "%s %s %d\n", buf1, op, &bb) == 3) {
    if (sscanf(buf3, "%s %s %s", buf1, op, buf2) == 3) {
      bigInit(buf1, &a);
      bigInit(buf2, &b);
    } else if (sscanf(buf3, "%s %s\n", op, buf2) == 2) {
      memset(buf1, '\0', 1024);
      bigInit("", &a);
      bigInit(buf2, &b);
    } else continue;

    bigInit("", &c);

    int cp;
    // 先利用 peek() 跳過開頭的 0
    switch (op[0]) {
      case '?': cp = compare(a, b); printf("%s\n", cp == 0?"equal":(cp>0?"greater":"less")); break;
      case '>': bigShiftRight(&c, b, 4); break;
      case '<': bigShiftLeft(&c, b, 4); break;
      case '|': absv(&c, b); break;
      case '~': toComp(&c, b); inc(&c); break;
      case '+': {
        if (op[1] == '+') { bigSet(&c, b); inc(&c); }
        else add(a, b, &c);
        break;
      }
      case '-': sub(a, b, &c); break;
      case '*': bigMul(a, b, &c); break;
      case '/': bigDiv(a, b, &c); break;
      case '%': modulo(a, b, &c); break;
    }

    if (op[0] != '?') {
      bigPrint(c); printf ("\n");
    }
  }
  return 0;
}
