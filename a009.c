#include <stdio.h>

int main (int argc, char* argv[])
{
  int c;
  while (EOF != (c = getchar())) {
    if (c == '\n') printf ("\n");
    else printf ("%c", (char)((c + 'C' - 'J')%255));
  }
  return 0;
}
