#include <stdio.h>

int main (int argc, char* argv[])
{
  int n, a[4], r;

  scanf ("%d", &n);
  for (int i=0; i<n; i++) {
    scanf ("%d %d %d %d", a, a+1, a+2, a+3);
    if ((a[3] - a[0]) == (3*(a[1] - a[0]))) r = a[3]+a[3]-a[2];
    else r = a[3]*a[3]/a[2];
    printf ("%d %d %d %d %d\n", a[0], a[1], a[2], a[3], r);
  }
  return 0;
}
