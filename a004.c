#include <stdio.h>

int main (int argc, char* argv[])
{
  int a;
  char m[2][10] = { "平", "閏" };
  while (scanf ("%d", &a) == 1) {
    printf ("%s年\n", m[!(a%400) || ((a%100) && !(a%4))]);
  }
  return 0;
}
