#include <stdio.h>

int check(int* a) {
  // 1) 比對直線(橫+直)
  for (int k=0; k<9; ++k) {
    for (int i=0; i<9; ++i) {
      for (int j=i+1; j<9; ++j) {
        if (*(a+k*9+i) == *(a+k*9+j)) return 0; // 橫線
        if (*(a+i*9+k) == *(a+j*9+k)) return 0; // 直線
      }
    }
  }
  // 3) 比對3x3
  for (int m=0; m<3; ++m) {
    for (int n=0; n<3; ++n) {
      // 比對每個 3x3
      for (int j=0; j<9; ++j) {
        for (int i=0; i<9; ++i) {
          if (i != j && *(a+(m*3+j/3)*9+j%3+n*3) == *(a+(m*3+i/3)*9+i%3+n*3)) return 0;
        }
      }
    }
  }
  return 1;
}

int main ()
{
  int a[81];
  int *pa;
  
  while (!feof(stdin)) {
    pa = a;
    for (int i=0; i<9; ++i) {
      if (scanf ("%d %d %d %d %d %d %d %d %d", pa+0, pa+1, pa+2, pa+3, pa+4, pa+5, pa+6, pa+7, pa+8) != 9) return 0;
      pa += 9;
    }
    printf ("%s\n", check(a)?"yes":"no");
  }
  return 0;
}
